import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InfosLivre {
    public JPanel panel1;
    private JLabel NomField;
    private JLabel PrenomField;
    private JLabel IsbnField;
    private JLabel idField;
    private JLabel TitreField;
    private JLabel AnneeFiield;
    private JButton supprimerButton;
    private JButton emprunterButton;

    public InfosLivre(String idUtilisateur,String Id ,String Isbn, String prenom,String Nom, String Titre, String Annee,Boolean cat){
        NomField.setText(Nom);
        PrenomField.setText(prenom);
        idField.setText(Id);
        TitreField.setText(Titre);
        AnneeFiield.setText(Annee);
        IsbnField.setText(Isbn);
        Livre l = new Livre(Integer.valueOf(Id));

        emprunterButton.addActionListener(e -> {
            try {
                l.emprunter(idUtilisateur);
            } catch (Exception throwables) {
                throwables.printStackTrace();
            }

        });
        if(cat){
        supprimerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                l.supprimer();
            }
        });}
        else {supprimerButton.setVisible(false);}
    }
}
