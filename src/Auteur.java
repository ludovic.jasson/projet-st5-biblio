import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Auteur {
    String Nom;
    String Prenom;
    public Integer IdAut;
    public Auteur(String Nom,String Prenom){
        this.Prenom=Prenom;
        this.Nom=Nom;
    }
    public void ajouter() throws SQLException {
        CoBDD c = new CoBDD();

            PreparedStatement ps = c.c.prepareStatement("INSERT INTO Auteur (Nom, Prenom)  SELECT ?,?FROM DUAL\n" +
                    "WHERE NOT EXISTS(\n" +
                    "        SELECT Nom FROM Auteur\n" +
                    "        WHERE Nom=?\n" +
                    "          AND Prenom =?\n" +
                    "    )" );
            ps.setString(1,Nom);
            ps.setString(2,Prenom);
            ps.setString(3,Nom);
            ps.setString(4,Prenom);
            ps.execute();
            ps = c.c.prepareStatement(
                    "        SELECT IdAut FROM Auteur\n" +
                    "        WHERE Nom=?\n" +
                    "          AND Prenom =?");
            ps.setString(1,Nom);
            ps.setString(2,Prenom);
            ResultSet rs = ps.executeQuery();
            rs.last();
            IdAut= rs.getInt("IdAut");

    }
}
