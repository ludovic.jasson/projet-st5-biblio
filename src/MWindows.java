import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

public class MWindows {
    JTabbedPane tabs;
    Boolean cat;
    int id;
    public MWindows(Boolean cat,int id){
        this.cat=cat;
        this.id=id;
    }
    public void init(){
        try {
            tabs = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);

            tabs.add("Livres Disponnibles",new ListeLivres(String.valueOf(id),cat).ListeLivesdispo);

            if (cat){tabs.add("Ajouter un Livre", new AjoutLivre().AjoutLivre);}

            tabs.add("Infos",new Infos(new Utilisateur(this.id)).infos);

            tabs.add("Emprunts en cours",new RendreLivre(new Utilisateur(id)).RendrePane);

            if (cat){tabs.add("Adhérents",new ListeUtilisateurs().ListeUtil);}
            JFrame frame = new JFrame("Bibliothèque");
            JPanel contentpane = (JPanel) frame.getContentPane();
            contentpane.add(tabs);
            UIManager.setLookAndFeel( new NimbusLookAndFeel());
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
            frame.setLocationRelativeTo( null );
        }
        catch (Exception e){e.printStackTrace();}
    }
    public static void main(String[] args) {
        MWindows m = new MWindows(true,2);
        m.init(); }
}
