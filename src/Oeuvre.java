import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Oeuvre {
    public Integer IdOeuvre;
    public String titre;
    public Integer annee;
    public Oeuvre(String titre,Integer annee){
        this.titre=titre;
        this.annee=annee;
    }
    public void ajouter() {
        try {
            CoBDD c = new CoBDD();
            PreparedStatement ps = c.c.prepareStatement(
                    "INSERT INTO Oeuvre (Titre, Annee) SELECT ?, ? FROM DUAL\n" +
                    "WHERE NOT EXISTS(\n" +
                    "        SELECT Titre FROM Oeuvre\n" +
                    "        WHERE Titre=?\n" +
                    "          AND Annee =? LIMIT 1\n" +
                    "    )");
            ps.setString(1, titre);
            ps.setInt(2, annee);
            ps.setString(3, titre);
            ps.setInt(4, annee);
            ps.execute();
            ps = c.c.prepareStatement("select IdOeuvre from Oeuvre where Titre=? and Annee=?");
            ps.setString(1, titre);
            ps.setInt(2, annee);
            ResultSet rs = ps.executeQuery();
            rs.last();
            IdOeuvre = rs.getInt("IdOeuvre");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }}

    public static void main(String[] args) {
        Oeuvre o = new Oeuvre("test",1234);
        o.ajouter();
    }
}
