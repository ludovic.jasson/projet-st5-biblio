import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Empruntspasse {
    public JPanel emprunts;
    public SQLTable table;
    public Empruntspasse(Integer id) throws UnsupportedLookAndFeelException {
        emprunts =new JPanel();
        UIManager.setLookAndFeel( new NimbusLookAndFeel());
        try {
            CoBDD c = new CoBDD();
            String sql ="select distinct E.ISBN,Nom,Prenom,Titre,date from Livre\n" +
                    "    join Edition E on Livre.ISBN = E.ISBN\n" +
                    "    join A_ecrit Ae on E.IdOeuvre = Ae.IdOeuvre\n" +
                    "    join Auteur A on Ae.IdAut = A.IdAut\n" +
                    "    join Oeuvre O on Ae.IdOeuvre = O.IdOeuvre\n" +
                    "    join Eut_emprunte Ee on E.ISBN = Ee.ISBN\n" +
                    "    where Ee.IdUtilisateur=?";
            PreparedStatement ps =c.c.prepareStatement(sql);
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            SQLTableModel rtm = new SQLTableModel( rs );
            SQLTable table = new SQLTable(rtm);
            emprunts.add( table, BorderLayout.CENTER );

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public static void main(String[] args) throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel( new NimbusLookAndFeel() );
        JFrame frame = new JFrame("Connexion");
        frame.setContentPane(new Empruntspasse(2).emprunts);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo( null );
    }
}
