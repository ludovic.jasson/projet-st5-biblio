import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ListeUtilisateurs {
    public JPanel ListeUtil = new JPanel();
    public SQLTable table;
    public ListeUtilisateurs() throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel( new NimbusLookAndFeel());
        try {
            CoBDD c = new CoBDD();
            String sql ="select * from Utilisateur where Nomcat='ADHERENT'";
            PreparedStatement ps =c.c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            SQLTableModel rtm = new SQLTableModel( rs );
            AdhTable table = new AdhTable(rtm);
            ListeUtil.add( table, BorderLayout.CENTER );

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public static void main(String[] args) throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel( new NimbusLookAndFeel() );
        JFrame frame = new JFrame("Connexion");
        frame.setContentPane(new ListeUtilisateurs().ListeUtil);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo( null );
    }

}
