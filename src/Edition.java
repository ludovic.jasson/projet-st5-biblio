import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Edition {
    public Integer IdOeuvre;
    public String ISBN;
    public Edition(String ISBN,Integer IdOeuvre){
        this.IdOeuvre=IdOeuvre;
        this.ISBN=ISBN;
    }
    public void ajouter() throws SQLException {
            CoBDD c = new CoBDD();
            PreparedStatement ps = c.c.prepareStatement("INSERT INTO Edition SELECT ?, ? FROM DUAL\n" +
                    "WHERE NOT EXISTS(\n" +
                    "        SELECT ISBN FROM Edition\n" +
                    "        WHERE ISBN=?\n" +
                    "          AND IdOeuvre =? LIMIT 1\n" +
                    "    )");
            ps.setString(1, ISBN);
            ps.setInt(2, IdOeuvre);
            ps.setString(3, ISBN);
            ps.setInt(4, IdOeuvre);
            ps.execute();
    }
}
