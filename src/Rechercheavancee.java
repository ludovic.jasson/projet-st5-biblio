import javax.swing.*;

public class Rechercheavancee {
    public JPanel panel1;
    private JTextField IsbnField;
    private JTextField NomField;
    private JTextField PrenomField;
    private JTextField Titrefield;
    private JButton lancerLaRechercheButton;
    public SQLTable table;

    public Rechercheavancee(String idUtilisateur,boolean id) {
        lancerLaRechercheButton.addActionListener(e -> {
            String Nom =NomField.getText();
            if (Nom.length()==0){Nom=" *";}
            String Prenom =PrenomField.getText();
            if (Prenom.length()==0){Prenom=" *";}
            String ISBN = IsbnField.getText();
            if (ISBN.length()==0){ISBN=" *";}
            String Titre = Titrefield.getText();
            if (Titre.length()==0){Titre=" *";}
            try {
                ListeLivres l =new ListeLivres(idUtilisateur,id);
                l.Rechercheavan(ISBN,Nom,Prenom,Titre);
                JPanel pane = l.ListeLivesdispo;
                JOptionPane.showConfirmDialog(null, pane, "recherche", JOptionPane.OK_CANCEL_OPTION);

            } catch (UnsupportedLookAndFeelException unsupportedLookAndFeelException) {
                unsupportedLookAndFeelException.printStackTrace();
            }


        });
    }
}
