import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public class SQLTableModel extends AbstractTableModel {
    private final ArrayList<String> columnsNames = new ArrayList<>();
    private final ArrayList< ArrayList<Object> > values = new ArrayList<>();

    public SQLTableModel( ResultSet resultSet ) throws SQLException {
        ResultSetMetaData rsmd = resultSet.getMetaData();
        int columnCounts = rsmd.getColumnCount();
        for( int i=1; i<= columnCounts; i++ ) {
            columnsNames.add( rsmd.getColumnName( i ) );

        }
        while( resultSet.next() ) {
            ArrayList<Object> line = new ArrayList<>();
            for( int i=1; i<= columnCounts; i++ ) {
                line.add( resultSet.getObject( i ) );
            }
            values.add( line );
        }


    }
    @Override public String getColumnName(int i) {
        return columnsNames.get( i );
    }

    @Override public int getColumnCount() {
        return columnsNames.size();
    }

    @Override public int getRowCount() {
        return values.size();
    }

    @Override public Object getValueAt( int line, int column ) {
        return values.get( line ).get( column );
    }

}

/*
    private ResultSet rs;
    private ResultSetMetaData rsMD;

    public SQLTableModel( ResultSet rs ){
        this.rs = rs;
        try {
            this.rsMD = rs.getMetaData();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public int getColumnCount() {
        try {
            return rsMD.getColumnCount();
        }
        catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
    @Override
    public int getRowCount(){
        try {
            rs.last();
            return rs.getRow();
        }
        catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            rs.absolute( rowIndex + 1 );
            return rs.getObject(columnIndex + 1 );
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    @Override
    public String getColumnName( int column ) {
        try {
            return rsMD.getColumnName( column + 1 );
        }
        catch (SQLException e){
            e.printStackTrace();
            return "";
        }
    }
}*/
