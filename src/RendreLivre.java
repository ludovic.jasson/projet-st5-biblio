import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

public class RendreLivre {
    private JComboBox comboBox1;
    private JButton rendreLeLivreButton;
    public JPanel RendrePane;

    public RendreLivre(Utilisateur a) {
        try {
            CoBDD c = new CoBDD();
            String sql = "select Idlivre,E.ISBN,Nom,Prenom,Titre,Annee from Livre\n" +
                    "    join Edition E on Livre.ISBN = E.ISBN\n" +
                    "    join A_ecrit Ae on E.IdOeuvre = Ae.IdOeuvre\n" +
                    "    join Auteur A on Ae.IdAut = A.IdAut\n" +
                    "    join Oeuvre O on Ae.IdOeuvre = O.IdOeuvre\n" +
                    "    where IdUtilisateur =?";
            PreparedStatement ps = c.c.prepareStatement(sql);
            ps.setString(1, a.id.toString());
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCounts = rsmd.getColumnCount();
            ArrayList<Object> Propositions = new ArrayList<Object>();
            while (rs.next()) {
                StringBuilder line = new StringBuilder();
                for (int i = 1; i <= columnCounts; i++) {
                    line.append(rs.getObject(i).toString());
                    line.append(",");
                }
                comboBox1.addItem(line.toString());
                Propositions.add(line);
            }
        }
        catch (Exception e){System.out.println(e);}


        rendreLeLivreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String res = (String) comboBox1.getSelectedItem();
                Integer b = Character.getNumericValue(res.charAt(0));

                a.rendreLivre(b);
            }
        });
    }

    public static void main(String[] args) throws Exception {
        JFrame frame = new JFrame("Connexion");
        frame.setContentPane(new RendreLivre(new Utilisateur(2)).RendrePane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo( null );
    }
}
