import java.sql.*;
import java.time.LocalDate;

public class Livre {
    static int idLivrecompteur = 4;
    static String table = "Livre";
    String ISBN;
    Integer idUtilisateur;
    Date dated;
    Date datef;
    Integer idLivre;
    public Livre(Integer idLivre, String ISBN, Integer idUtilisateur, Date dated, Date datef){
        this.ISBN = ISBN;
        this.idUtilisateur = idUtilisateur;
        this.dated=dated;
        this.datef=datef;
        this.idLivre=idLivre;
        if (idLivre == null){
            idLivrecompteur++;
            this.idLivre=idLivrecompteur;
        }
    }
    public Livre(Integer idLivre){
        CoBDD c = new CoBDD();
        try {
            PreparedStatement ps = c.c.prepareStatement("select * from Livre where Idlivre=?");
            ps.setInt(1,idLivre);
            ResultSet rs =ps.executeQuery();
            rs.last();
            this.ISBN = rs.getString("ISBN");
            this.idUtilisateur = rs.getInt("idUtilisateur");
            this.dated = rs.getDate("dated");
            this.datef = rs.getDate("datef");
            this.idLivre=rs.getInt("Idlivre");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public Livre(String ISBN){
        this.ISBN=ISBN;

    }
    public void inserLivre(){
        CoBDD c = new CoBDD();
        try {
            PreparedStatement ps = c.c.prepareStatement("insert into Livre (ISBN) values (?)");
            ps.setString(1,ISBN);
            ps.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void supprimer(){
        CoBDD c =new CoBDD();
        String condition = "idLivre = "+ this.idLivre;
        c.Supprimer(table,condition);
    }
    public void changerLivre(Integer idUtilisateur,Date dated,Date datef){
        try {
            CoBDD c= new CoBDD();
            PreparedStatement ps;
            if (idUtilisateur==null){
                ps = c.c.prepareStatement("UPDATE Livre set IdUtilisateur=null,dated=null,datef=null where Idlivre=?");
                ps.setInt(1,idLivre);
            }
            else {
                ps = c.c.prepareStatement("UPDATE Livre set IdUtilisateur=?,dated=?,datef=? where Idlivre=?");
                ps.setString(1, idUtilisateur.toString());
                ps.setDate(2, dated);
                ps.setDate(3, datef);
                ps.setString(4, idLivre.toString());
            }
            ps.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    public void rendreLivre(){
        changerLivre(null,null,null);
    }
    public void emprunter(String idUtilisateur) throws Exception {
        Utilisateur u = new Utilisateur(Integer.valueOf(idUtilisateur));
        String nomcat = u.Nomcat;
        CoBDD c = new CoBDD();
        PreparedStatement ps = c.c.prepareStatement("select * from Categorie where Nomcat=?");
        ps.setString(1,nomcat);
        ResultSet rs = ps.executeQuery();
        rs.last();
        int duree =rs.getInt("Dureemax");
        int max =rs.getInt("Nombreemprunt");
        ps = c.c.prepareStatement("select count(*) from Livre where IdUtilisateur=?");
        ps.setString(1,idUtilisateur);
        rs = ps.executeQuery();
        rs.last();
        int count = rs.getInt(1);
        System.out.println(count);
        if (count<max){
            System.out.println("emprnt");
        LocalDate localDate = LocalDate.now();
        LocalDate datefin = LocalDate.now().plusDays(duree);
        changerLivre(Integer.valueOf(idUtilisateur),java.sql.Date.valueOf(localDate),java.sql.Date.valueOf(datefin));
    }}

}
