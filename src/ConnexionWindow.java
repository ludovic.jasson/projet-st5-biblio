import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConnexionWindow {
    private JTextField id;
    public JPanel ConnexionPannel;
    private JPasswordField mdpfield;
    private JButton connexionButton;
    private JLabel Verif;

    public ConnexionWindow() {
        connexionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int identifiant = Integer.parseInt(id.getText());
                String mdp = new String(mdpfield.getPassword());
                Connexion connexion = new Connexion();
                String verif = connexion.tentative(identifiant,mdp);
                Verif.setText(verif);
                if (verif.equals("ADHERENT")){
                    MWindows mw= new MWindows(false,identifiant);
                    mw.init();
                }
                if (verif.equals("ADMIN")){
                    MWindows mw= new MWindows(true,identifiant);
                    mw.init();
                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Connexion");
        frame.setContentPane(new ConnexionWindow().ConnexionPannel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo( null );
    }
}
