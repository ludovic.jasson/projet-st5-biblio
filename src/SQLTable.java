import java.awt.BorderLayout;

import javax.swing.*;


public class SQLTable extends JPanel {
    public JTable table;
    public SQLTable( SQLTableModel model ) {
        table = new JTable( model );
        table.setAutoCreateRowSorter( true );
        setLayout( new BorderLayout() );
        add( new JScrollPane( table ), BorderLayout.CENTER );
    }
}
