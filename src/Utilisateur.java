import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Utilisateur {
    public Integer id;
    public String nom;
    public String prenom;
    public String email;
    public String mdp;
    public String table = "Utilisateur";
    public String Nomcat;
    public Utilisateur(String nom, String prenom, String email, String mdp,Boolean cat){
        if (cat) {
            this.Nomcat="ADMIN";
        }
        else {
            this.Nomcat="ADHERENT";
        }
        this.nom=nom;
        this.prenom=prenom;
        this.email=email;
        this.mdp=mdp;
    }
    public Utilisateur(String id) throws Exception {
        Utilisateur u = new Utilisateur(Integer.valueOf(id));
        this.Nomcat=u.Nomcat;
        this.email=u.email;
        this.id=u.id;
        this.mdp=u.mdp;
        this.nom=u.nom;
        this.prenom=u.prenom;
    }
    public Utilisateur(Integer id) throws Exception {
        CoBDD c= new CoBDD();
        ResultSet rs;
        String condition = String.format("Idutilisateur = %s",id.toString());
        rs = c.Recherche(table, "*", condition);
        rs.last();
        this.nom = rs.getString("Nom");
        this.prenom = rs.getString("Prenom");
        this.email = rs.getString("email");
        this.mdp = rs.getString("mdp");
        this.Nomcat=rs.getString("Nomcat");

        this.id=id;
        }
    public void ajouter() throws NoSuchAlgorithmException {
        hachmdp h =new hachmdp();
        this.mdp=h.hach(this.mdp);
        CoBDD c = new CoBDD();
        String elements = "Nom,Prenom,email,Nomcat,mdp";
        String values = String.format("'%s','%s','%s','%s','%s'",this.nom, this.prenom,this.email ,this.Nomcat,this.mdp) ;
        c.insertion(table,elements,values);
    }
    public void rendreLivre(Integer idLivre){
        Livre l = new Livre(idLivre);
        String Isbn = l.ISBN;
        Date date = l.dated;
        l.rendreLivre();
        new EutEmprunte(Isbn,this.id,date);
    }
    public  void nouveaumdp(String mdp){
        CoBDD c= new CoBDD();
        try {
            PreparedStatement ps = c.c.prepareStatement("UPDATE Utilisateur SET mdp = ? WHERE IdUtilisateur = ?");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
    public String supprimer(){
        CoBDD c =new CoBDD();
        try {
                c.Supprimer("Livre",String.format("IdUtilisateur ='%s'",this.id.toString()));
                c.Supprimer(table,String.format("IdUtilisateur ='%s'",this.id.toString()));
                c.Supprimer("Eut_emprunte",String.format("IdUtilisateur ='%s'",this.id.toString()));
                return "Utilisateur supprimé";

        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    return "erreur";}

    public static void main(String[] args) throws NoSuchAlgorithmException {
        Utilisateur a=new Utilisateur("J","L","L.J@gmail.com","m",false);
        a.ajouter();
    }
}

