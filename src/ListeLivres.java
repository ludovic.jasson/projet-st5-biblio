import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ListeLivres {
    public JPanel ListeLivesdispo;
    public SQLTable table;
    String idUtilisateur;
    Boolean cat;
    public ListeLivres(String idUtilisateur,boolean cat) throws UnsupportedLookAndFeelException {
        this.idUtilisateur=idUtilisateur;
        this.cat=cat;
        ListeLivesdispo = new JPanel();
        UIManager.setLookAndFeel(new NimbusLookAndFeel());
        tableau();
        table.table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    JTable target = (JTable)me.getSource();
                    int row = target.getSelectedRow();
                    InfosLivre il = new InfosLivre(idUtilisateur,table.table.getValueAt(row, 0).toString(),table.table.getValueAt(row, 1).toString(),table.table.getValueAt(row, 2).toString(),table.table.getValueAt(row, 3).toString(),table.table.getValueAt(row, 4).toString(),table.table.getValueAt(row, 5).toString(),cat);
                    ListeLivesdispo.add( il.panel1, BorderLayout.LINE_START );
                    ListeLivesdispo.add( table, BorderLayout.CENTER );
                    ListeLivesdispo.revalidate();
                    ListeLivesdispo.repaint();
                }}});
        ListeLivesdispo.add( table, BorderLayout.CENTER );
        ListeLivesdispo.add(new Rechercheavancee(idUtilisateur,cat).panel1,BorderLayout.LINE_END);

    }

    public void tableau(){
            try {
                CoBDD c = new CoBDD();
                String sql = "select Idlivre,E.ISBN,Nom,Prenom,Titre,Annee from Livre\n" +
                        "    join Edition E on Livre.ISBN = E.ISBN\n" +
                        "    join A_ecrit Ae on E.IdOeuvre = Ae.IdOeuvre\n" +
                        "    join Auteur A on Ae.IdAut = A.IdAut\n" +
                        "    join Oeuvre O on Ae.IdOeuvre = O.IdOeuvre where dated is null";
                PreparedStatement ps =c.c.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                SQLTableModel rtm = new SQLTableModel( rs );
                table = new SQLTable(rtm);

            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    public void Rechercheavan(String ISBN,String Nom,String Prenom,String Titre){
        try {
            CoBDD c = new CoBDD();
            String sql = "select Idlivre,E.ISBN,Nom,Prenom,Titre,Annee from Livre\n" +
                    "    join Edition E on Livre.ISBN = E.ISBN\n" +
                    "    join A_ecrit Ae on E.IdOeuvre = Ae.IdOeuvre\n" +
                    "    join Auteur A on Ae.IdAut = A.IdAut\n" +
                    "    join Oeuvre O on Ae.IdOeuvre = O.IdOeuvre where E.ISBN regexp ? and Nom regexp ? and Prenom regexp ? and Titre regexp ?";
            PreparedStatement ps =c.c.prepareStatement(sql);
            ps.setString(1,ISBN);
            ps.setString(2,Nom);
            ps.setString(3,Prenom);
            ps.setString(4,Titre);
            ResultSet rs = ps.executeQuery();
            SQLTableModel rtm = new SQLTableModel( rs );
            table = new SQLTable(rtm);
            ListeLivesdispo.removeAll();
            ListeLivesdispo.revalidate();
            UIManager.setLookAndFeel(new NimbusLookAndFeel());
            table.table.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent me) {
                    if (me.getClickCount() == 2) {
                        JTable target = (JTable)me.getSource();
                        int row = target.getSelectedRow();
                        InfosLivre il = new InfosLivre(idUtilisateur,table.table.getValueAt(row, 0).toString(),table.table.getValueAt(row, 1).toString(),table.table.getValueAt(row, 2).toString(),table.table.getValueAt(row, 3).toString(),table.table.getValueAt(row, 4).toString(),table.table.getValueAt(row, 5).toString(),cat);
                        ListeLivesdispo.add( il.panel1, BorderLayout.LINE_START );
                        ListeLivesdispo.add( table, BorderLayout.CENTER );
                        ListeLivesdispo.revalidate();
                        ListeLivesdispo.repaint();
                    }}});
            ListeLivesdispo.add( table, BorderLayout.CENTER );


        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public static void main(String[] args) throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel( new NimbusLookAndFeel() );
        JFrame frame = new JFrame("Connexion");
        frame.setContentPane(new ListeLivres("2",true).ListeLivesdispo);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo( null );
    }
}
