import java.sql.PreparedStatement;
import java.sql.SQLException;

public class A_ecrit {
    public Integer IdOeuvre;
    public Integer idAut;
    public A_ecrit(Integer idAut,Integer IdOeuvre){
        this.IdOeuvre=IdOeuvre;
        this.idAut=idAut;
    }
    public void ajouter() {
        try {
            CoBDD c = new CoBDD();
            PreparedStatement ps = c.c.prepareStatement(
                    "INSERT INTO A_ecrit SELECT ?, ? FROM DUAL\n" +
                            "WHERE NOT EXISTS(\n" +
                            "        SELECT IdOeuvre FROM A_ecrit\n" +
                            "        WHERE IdOeuvre=?\n" +
                            "          AND IdAut =? LIMIT 1\n" +
                            "    )");
            ps.setInt(2, idAut);
            ps.setInt(1, IdOeuvre);
            ps.setInt(4, idAut);
            ps.setInt(3, IdOeuvre);
            ps.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void main(String[] args) {
    A_ecrit a=new A_ecrit(3,4);
    a.ajouter();
    }
}
