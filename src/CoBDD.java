import java.sql.*;

public class CoBDD {
    public Connection c;
    public PreparedStatement ps = null;
    public ResultSet rs = null;

    public CoBDD() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/BDDMYSQL","admin","root");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void insertion(String table, String column, String values) {
        try {
            String sql =String.format("INSERT INTO %s (%s) VALUES (%s)",table,column,values);
            ps = c.prepareStatement(sql);
            ps.execute();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }
    public void Supprimer(String table, String conditions) {
        try {
            String sql = String.format("DELETE FROM %s WHERE %s",table,conditions);//
            ps = c.prepareStatement(sql);
            ps.execute();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }
    public ResultSet Recherche(String table, String elements, String conditions) throws Exception{
        try {
            String sql = String.format("SELECT %s from %s WHERE %s",elements,table,conditions);
            ps = c.prepareStatement(sql);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            throw new Exception(e);
        }
        return rs;
    }
}

