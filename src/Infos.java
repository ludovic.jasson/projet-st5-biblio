import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Infos {
    public JPanel infos;
    private JLabel id;
    private JLabel Nom;
    private JLabel Prenom;
    private JLabel email;
    private JLabel Nomcat;
    private JPanel Liste;
    private JButton nouveauMotDePasseButton;
    private JButton supprimerUtilisateurButton;

    public Infos(Utilisateur a) throws UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel( new NimbusLookAndFeel());
        id.setText(a.id.toString());
        Nom.setText(a.nom);
        Prenom.setText(a.prenom);
        email.setText(a.email);
        Nomcat.setText(a.Nomcat);
        Liste.add(new Empruntspasse(a.id).emprunts);
        nouveauMotDePasseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String mdp = JOptionPane.showInputDialog("Nouveau mot de passe");
                a.nouveaumdp(mdp);

            }
        });
        if (Nomcat.equals("ADHERENT")){
            supprimerUtilisateurButton.setVisible(false);
        }
        supprimerUtilisateurButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int dialogResult = JOptionPane.showConfirmDialog (null, "Supprimer l'utilisateur ?",null,JOptionPane.YES_NO_OPTION);
                if(dialogResult == JOptionPane.YES_OPTION){
                    a.supprimer();
                }
            }
        });
    }
}
