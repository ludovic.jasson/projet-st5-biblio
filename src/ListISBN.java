import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

public class ListISBN {
    private ArrayList<String> Propositions = new ArrayList<>();
    public String[] ArrayPorposition;
    public ListISBN() {
        try {
            CoBDD c = new CoBDD();
            String sql = "SELECT distinct isbn from Livre";
            PreparedStatement ps = c.c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Propositions.add(rs.getObject(0).toString());
            }
            ArrayPorposition =new String[Propositions.size()];
            ArrayPorposition =Propositions.toArray(ArrayPorposition);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}