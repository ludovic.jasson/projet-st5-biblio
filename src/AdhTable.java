import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdhTable extends JPanel{
    private final JTable table;
    JPanel info;
    JScrollPane Sinfo;
    public AdhTable( SQLTableModel model ) {
        setLayout( new BorderLayout() );
        table = new JTable( model );
        table.setAutoCreateRowSorter( true );
        add( new JScrollPane( table ), BorderLayout.CENTER );
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    removeAll();
                    validate();
                    JTable target = (JTable)me.getSource();
                    int row = target.getSelectedRow();
                    String id = table.getValueAt(row, 0).toString();
                    try {
                        Utilisateur u = new Utilisateur(Integer.valueOf(id));
                        info = new Infos(u).infos;
                        Sinfo = new JScrollPane(info);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    add( new JScrollPane( table ), BorderLayout.CENTER );
                    add(new JScrollPane(info),BorderLayout.EAST);
                    revalidate();
                    repaint();
                }
                if (SwingUtilities.isRightMouseButton(me)){
                    JOptionPane.showConfirmDialog(null, new AjouterAdherent().panel1, "Ajouter un Adhérent", JOptionPane.OK_CANCEL_OPTION);
                    revalidate();
                    repaint();

                }
            }

        });


    }
}
