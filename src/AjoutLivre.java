import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;


public class AjoutLivre {
    public JPanel AjoutLivre;
    private JTextField textField1;
    private JButton ajouterLivreButton;

    public AjoutLivre() {
        //ListISBN l = new ListISBN();
        ajouterLivreButton.addActionListener(e -> {
            String ISBN = textField1.getText();
            try {
                InserreNouveaulivre inserreNouveaulivre= new InserreNouveaulivre(ISBN);
            } catch (IOException | SQLException ioException) {
                ioException.printStackTrace();
            }
        });
    }
}