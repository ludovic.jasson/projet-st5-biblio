import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.SQLException;

public class InserreNouveaulivre {
    public InserreNouveaulivre(String ISBN) throws IOException, SQLException {
        JSONObject json = JsonReader.readJsonFromUrl(String.format("https://www.googleapis.com/books/v1/volumes?q=isbn:%s",ISBN));
        String titre = json.getJSONArray("items").getJSONObject(0).getJSONObject("volumeInfo").getString("title");
        JSONArray auteurs =json.getJSONArray("items").getJSONObject(0).getJSONObject("volumeInfo").getJSONArray("authors");
        String Annee = (String) json.getJSONArray("items").getJSONObject(0).getJSONObject("volumeInfo").get("publishedDate");
        Oeuvre o =new Oeuvre(titre,Integer.valueOf(Annee));
        o.ajouter();
        Integer idOeuvre =o.IdOeuvre;
        Edition e = new Edition(ISBN,idOeuvre);
        e.ajouter();
        for (int i=0; i < auteurs.length(); i++){
            String[] PrenomNom = auteurs.getString(i).split("\\s+");
            Auteur a = new Auteur(PrenomNom[1], PrenomNom[0]);
            a.ajouter();
            Integer idAut = a.IdAut;
            A_ecrit a_ecrit = new A_ecrit(idAut, idOeuvre);
            a_ecrit.ajouter();
        }
        Livre l=new Livre(ISBN);
        l.inserLivre();
    }

    public static void main(String[] args) throws IOException, SQLException {
        InserreNouveaulivre i=new InserreNouveaulivre("9780670805112");
    }
}
