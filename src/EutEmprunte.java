import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EutEmprunte {
    public EutEmprunte (String ISBN, Integer idUtilisateur, Date date){
        try {
            CoBDD c= new CoBDD();
            PreparedStatement ps = c.c.prepareStatement("insert into Eut_emprunte values(?,?,?)");
            ps.setString(1,ISBN);
            ps.setInt(2,idUtilisateur);
            ps.setDate(3,date);
            ps.execute();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }}